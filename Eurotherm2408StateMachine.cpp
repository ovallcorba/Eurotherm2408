/*----- PROTECTED REGION ID(Eurotherm2408StateMachine.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        Eurotherm2408StateMachine.cpp
//
// description : C++ source for the �name� and its alowed
//               methods for commands and attributes
//
// project :     Eurotherm2408.
//
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// SVN only:
// $HeadURL:  $
//
// CVS only:
// $Source:  $
// $Log:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================



#include <Eurotherm2408.h>
#include <Eurotherm2408Class.h>

/*----- PROTECTED REGION END -----*/


/*
 * Eurotherm2408 states description:
 *
 * ON :	Everything ok 
 * FAULT :	Something went wrong 
 * ALARM :	 
 */

namespace Eurotherm2408_ns
{

//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_AdaptiveTuneState_allowed()
 *	Description : Execution allowed for AdaptiveTune attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_AdaptiveTune_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for AdaptiveTune attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::AdaptiveTuneStateAllowed_WRITE) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::AdaptiveTuneStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for AdaptiveTune attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_AdaptiveTuneStateAllowed_READ) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_AdaptiveTuneStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_AllowedStatesState_allowed()
 *	Description : Execution allowed for AllowedStates attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_AllowedStates_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
		if (	//	Compare device state with not allowed states for WRITE 
			get_state() == Tango::FAULT)
		{
		
	/*----- PROTECTED REGION ID(Eurotherm2408::AllowedStatesStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::AllowedStatesStateAllowed_WRITE

			return false;
		}
		return true;
	}
	else
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_AllowedStatesStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_AllowedStatesStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_AutoTuneState_allowed()
 *	Description : Execution allowed for AutoTune attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_AutoTune_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for AutoTune attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::AutoTuneStateAllowed_WRITE) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::AutoTuneStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for AutoTune attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_AutoTuneStateAllowed_READ) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_AutoTuneStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_CurrentProgramState_allowed()
 *	Description : Execution allowed for CurrentProgram attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_CurrentProgram_allowed(Tango::AttReqType type)
{
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_CurrentProgramStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_CurrentProgramStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_CurrentSegmentState_allowed()
 *	Description : Execution allowed for CurrentSegment attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_CurrentSegment_allowed(Tango::AttReqType type)
{
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_CurrentSegmentStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_CurrentSegmentStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_CurrentSetPointState_allowed()
 *	Description : Execution allowed for CurrentSetPoint attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_CurrentSetPoint_allowed(Tango::AttReqType type)
{
		//	Not any excluded states for CurrentSetPoint attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_CurrentSetPointStateAllowed_READ) ENABLED START -----*/
	if (get_state() == Tango::FAULT)
			return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_CurrentSetPointStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_HoldBackState_allowed()
 *	Description : Execution allowed for HoldBack attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_HoldBack_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
		if (	//	Compare device state with not allowed states for WRITE 
			get_state() == Tango::FAULT)
		{
		
	/*----- PROTECTED REGION ID(Eurotherm2408::HoldBackStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::HoldBackStateAllowed_WRITE

			return false;
		}
		return true;
	}
	else
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_HoldBackStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_HoldBackStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_ManualState_allowed()
 *	Description : Execution allowed for Manual attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_Manual_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
		if (	//	Compare device state with not allowed states for WRITE 
			get_state() == Tango::FAULT)
		{
		
	/*----- PROTECTED REGION ID(Eurotherm2408::ManualStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::ManualStateAllowed_WRITE

			return false;
		}
		return true;
	}
	else
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_ManualStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_ManualStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_MaxProgramsState_allowed()
 *	Description : Execution allowed for MaxPrograms attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_MaxPrograms_allowed(Tango::AttReqType type)
{
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_MaxProgramsStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_MaxProgramsStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_OutputLevelState_allowed()
 *	Description : Execution allowed for OutputLevel attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_OutputLevel_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
		if (	//	Compare device state with not allowed states for WRITE 
			get_state() == Tango::FAULT)
		{
		
	/*----- PROTECTED REGION ID(Eurotherm2408::OutputLevelStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::OutputLevelStateAllowed_WRITE

			return false;
		}
		return true;
	}
	else
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_OutputLevelStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_OutputLevelStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_OutputLevelMaxState_allowed()
 *	Description : Execution allowed for OutputLevelMax attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_OutputLevelMax_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
		if (	//	Compare device state with not allowed states for WRITE 
			get_state() == Tango::FAULT)
		{
		
	/*----- PROTECTED REGION ID(Eurotherm2408::OutputLevelMaxStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::OutputLevelMaxStateAllowed_WRITE

			return false;
		}
		return true;
	}
	else
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_OutputLevelMaxStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_OutputLevelMaxStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_OutputLevelMinState_allowed()
 *	Description : Execution allowed for OutputLevelMin attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_OutputLevelMin_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
		if (	//	Compare device state with not allowed states for WRITE 
			get_state() == Tango::FAULT)
		{
		
	/*----- PROTECTED REGION ID(Eurotherm2408::OutputLevelMinStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::OutputLevelMinStateAllowed_WRITE

			return false;
		}
		return true;
	}
	else
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_OutputLevelMinStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_OutputLevelMinStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_PID2_DerivativeTimeState_allowed()
 *	Description : Execution allowed for PID2_DerivativeTime attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_PID2_DerivativeTime_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for PID2_DerivativeTime attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::PID2_DerivativeTimeStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::PID2_DerivativeTimeStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for PID2_DerivativeTime attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_PID2_DerivativeTimeStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_PID2_DerivativeTimeStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_PID2_IntegralTimeState_allowed()
 *	Description : Execution allowed for PID2_IntegralTime attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_PID2_IntegralTime_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for PID2_IntegralTime attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::PID2_IntegralTimeStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::PID2_IntegralTimeStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for PID2_IntegralTime attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_PID2_IntegralTimeStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_PID2_IntegralTimeStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_PID2_ProportionalBandState_allowed()
 *	Description : Execution allowed for PID2_ProportionalBand attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_PID2_ProportionalBand_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for PID2_ProportionalBand attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::PID2_ProportionalBandStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::PID2_ProportionalBandStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for PID2_ProportionalBand attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_PID2_ProportionalBandStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_PID2_ProportionalBandStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_PID_CutbackHighState_allowed()
 *	Description : Execution allowed for PID_CutbackHigh attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_PID_CutbackHigh_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for PID_CutbackHigh attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::PID_CutbackHighStateAllowed_WRITE) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::PID_CutbackHighStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for PID_CutbackHigh attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_PID_CutbackHighStateAllowed_READ) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_PID_CutbackHighStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_PID_CutbackLowState_allowed()
 *	Description : Execution allowed for PID_CutbackLow attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_PID_CutbackLow_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for PID_CutbackLow attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::PID_CutbackLowStateAllowed_WRITE) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::PID_CutbackLowStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for PID_CutbackLow attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_PID_CutbackLowStateAllowed_READ) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_PID_CutbackLowStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_PID_DerivativeTimeState_allowed()
 *	Description : Execution allowed for PID_DerivativeTime attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_PID_DerivativeTime_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for PID_DerivativeTime attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::PID_DerivativeTimeStateAllowed_WRITE) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::PID_DerivativeTimeStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for PID_DerivativeTime attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_PID_DerivativeTimeStateAllowed_READ) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_PID_DerivativeTimeStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_PID_IntegralTimeState_allowed()
 *	Description : Execution allowed for PID_IntegralTime attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_PID_IntegralTime_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for PID_IntegralTime attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::PID_IntegralTimeStateAllowed_WRITE) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::PID_IntegralTimeStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for PID_IntegralTime attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_PID_IntegralTimeStateAllowed_READ) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_PID_IntegralTimeStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_PID_ProportionalBandState_allowed()
 *	Description : Execution allowed for PID_ProportionalBand attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_PID_ProportionalBand_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for PID_ProportionalBand attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::PID_ProportionalBandStateAllowed_WRITE) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;
	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::PID_ProportionalBandStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for PID_ProportionalBand attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_PID_ProportionalBandStateAllowed_READ) ENABLED START -----*/
		if (get_state() == Tango::FAULT)
				return false;

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_PID_ProportionalBandStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_ProgramDirState_allowed()
 *	Description : Execution allowed for ProgramDir attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_ProgramDir_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
		if (	//	Compare device state with not allowed states for WRITE 
			get_state() == Tango::FAULT)
		{
		
	/*----- PROTECTED REGION ID(Eurotherm2408::ProgramDirStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::ProgramDirStateAllowed_WRITE

			return false;
		}
		return true;
	}
	else
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_ProgramDirStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_ProgramDirStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_ProgramStateState_allowed()
 *	Description : Execution allowed for ProgramState attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_ProgramState_allowed(Tango::AttReqType type)
{
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_ProgramStateStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_ProgramStateStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_RemainingProgramTimeState_allowed()
 *	Description : Execution allowed for RemainingProgramTime attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_RemainingProgramTime_allowed(Tango::AttReqType type)
{
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_RemainingProgramTimeStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_RemainingProgramTimeStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_RemainingSegmentTimeState_allowed()
 *	Description : Execution allowed for RemainingSegmentTime attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_RemainingSegmentTime_allowed(Tango::AttReqType type)
{
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_RemainingSegmentTimeStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_RemainingSegmentTimeStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_SegmentTypeState_allowed()
 *	Description : Execution allowed for SegmentType attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_SegmentType_allowed(Tango::AttReqType type)
{
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_SegmentTypeStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_SegmentTypeStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_SetpointState_allowed()
 *	Description : Execution allowed for Setpoint attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_Setpoint_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
		if (	//	Compare device state with not allowed states for WRITE 
			get_state() == Tango::FAULT)
		{
		
	/*----- PROTECTED REGION ID(Eurotherm2408::SetpointStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::SetpointStateAllowed_WRITE

			return false;
		}
		return true;
	}
	else
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_SetpointStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_SetpointStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_TemperatureState_allowed()
 *	Description : Execution allowed for Temperature attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_Temperature_allowed(Tango::AttReqType type)
{
	if (	//	Compare device state with not allowed states for READ 
		get_state() == Tango::FAULT)
	{
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_TemperatureStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_TemperatureStateAllowed_READ

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_GainSchedulerSetPointState_allowed()
 *	Description : Execution allowed for GainSchedulerSetPoint attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_GainSchedulerSetPoint_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for GainSchedulerSetPoint attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::GainSchedulerSetPointStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::GainSchedulerSetPointStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for GainSchedulerSetPoint attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_GainSchedulerSetPointStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_GainSchedulerSetPointStateAllowed_READ

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_GainSchedulerEnableState_allowed()
 *	Description : Execution allowed for GainSchedulerEnable attribute.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_GainSchedulerEnable_allowed(Tango::AttReqType type)
{
	//	Check if access type.
	if ( type!=Tango::READ_REQ )
	{
			//	Not any excluded states for GainSchedulerEnable attribute in WRITE access.
		
	/*----- PROTECTED REGION ID(Eurotherm2408::GainSchedulerEnableStateAllowed_WRITE) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::GainSchedulerEnableStateAllowed_WRITE

		return true;
	}
	else
		//	Not any excluded states for GainSchedulerEnable attribute in READ access.
	
	/*----- PROTECTED REGION ID(Eurotherm2408::read_GainSchedulerEnableStateAllowed_READ) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::read_GainSchedulerEnableStateAllowed_READ

	return true;
}


	/*----- PROTECTED REGION ID(Eurotherm2408::are_dynamic_attributes_allowed) ENABLED START -----*/

	//	Add your code to check if dynamic attributes are alowed

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::are_dynamic_attributes_allowed


//=================================================
//		Commands Allowed Methods
//=================================================


//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_StartProgramState_allowed()
 *	Description : Execution allowed for StartProgram command.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_StartProgram_allowed(const CORBA::Any &any)
{
	if (	//	Compare device state with not allowed states for command 
		get_state() == Tango::FAULT)
	{

	/*----- PROTECTED REGION ID(Eurotherm2408::StartProgramStateAllowed) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::StartProgramStateAllowed

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_StopProgramState_allowed()
 *	Description : Execution allowed for StopProgram command.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_StopProgram_allowed(const CORBA::Any &any)
{
	if (	//	Compare device state with not allowed states for command 
		get_state() == Tango::FAULT)
	{

	/*----- PROTECTED REGION ID(Eurotherm2408::StopProgramStateAllowed) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::StopProgramStateAllowed

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_HoldProgramState_allowed()
 *	Description : Execution allowed for HoldProgram command.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_HoldProgram_allowed(const CORBA::Any &any)
{
	if (	//	Compare device state with not allowed states for command 
		get_state() == Tango::FAULT)
	{

	/*----- PROTECTED REGION ID(Eurotherm2408::HoldProgramStateAllowed) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::HoldProgramStateAllowed

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_LoadProgramState_allowed()
 *	Description : Execution allowed for LoadProgram command.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_LoadProgram_allowed(const CORBA::Any &any)
{
	if (	//	Compare device state with not allowed states for command 
		get_state() == Tango::FAULT)
	{

	/*----- PROTECTED REGION ID(Eurotherm2408::LoadProgramStateAllowed) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::LoadProgramStateAllowed

		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Eurotherm2408::is_DeleteProgramState_allowed()
 *	Description : Execution allowed for DeleteProgram command.
 */
//--------------------------------------------------------

bool Eurotherm2408::is_DeleteProgram_allowed(const CORBA::Any &any)
{
	if (	//	Compare device state with not allowed states for command 
		get_state() == Tango::FAULT)
	{

	/*----- PROTECTED REGION ID(Eurotherm2408::DeleteProgramStateAllowed) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::DeleteProgramStateAllowed

		return false;
	}
	return true;
}


	/*----- PROTECTED REGION ID(Eurotherm2408::are_dynamic_commands_allowed) ENABLED START -----*/

	//	Add your code to check if dynamic commands are alowed

	/*----- PROTECTED REGION END -----*/	//	Eurotherm2408::are_dynamic_commands_allowed

}	// namespace Eurotherm2408_ns
