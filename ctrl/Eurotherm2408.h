#ifndef EUROTHERM_H
#define EUROTHERM_H

#include <vector>
#include <fstream>

extern "C"
{
	#include <pthread.h>
}
#include <cstring>
#include <climits>
#include <cstdlib>
#include <ipc_tty.h>
#include <ipc_tcp.h>
#include <ipc_exception.h>
#include <io.h>
#include "EurothermProgram.h"


class Eurotherm2408ctrl : public Io
{

	public:
		Eurotherm2408ctrl(unsigned short devaddr, bool logicOps, TtyDevice* device);
		Eurotherm2408ctrl(unsigned short devaddr, bool logicOps, TcpSocket* device);
		~Eurotherm2408ctrl();
		void			init();
		void			temperature(double* val);
		void			setPoint(double val);
		void			setPoint(double* val);
		void			setPointLowLimit(double val);
		void			setPointHighLimit(double val);
		void			setPointLowLimit(double* val);
		void			setPointHighLimit(double* val);
		void			multiplier(double val);
		void			multiplier(double* val);
		bool 			status(string& status_str, unsigned char allowed = 0);
		void 			outputStatus(unsigned short* word);
		void			manual(bool onoff);
		void			manual(bool* onoff);
		void			holdBack(bool onoff);
		void			holdBack(bool* onoff);
		void			highPowerLimit(double l);
		void			highPowerLimit(double *l);
		void			lowPowerLimit(double l);
		void			lowPowerLimit(double *l);
		void			outputLevel(double *l);
		void			outputLevel(double l);
		void			outputRateLimit(bool* onoff);
		void			outputRateLimit(bool onoff);
		void 			proportionalBand_PID1(double val);
		void 			proportionalBand_PID1(double* val);
		void 			proportionalBand_PID2(double val);
		void 			proportionalBand_PID2(double* val);
		void			integralTime_PID1(double val);
		void			integralTime_PID1(double* val);
		void			integralTime_PID2(double val);
		void			integralTime_PID2(double* val);
		void			derivativeTime_PID1(double val);
		void			derivativeTime_PID1(double* val);
	    void			derivativeTime_PID2(double val);
		void			derivativeTime_PID2(double* val);
		void			cutbackHigh(double val);
		void			cutbackHigh(double* val);
		void			cutbackLow(double val);
		void			cutbackLow(double* val);
		void 			autotune(bool onoff);
		void 			autotune(bool* onoff);
		void 			adaptiveTune(bool onoff);
		void 			adaptiveTune(bool* onoff);
        void            gainSchedulerSetPoint(double val);
		void            gainSchedulerSetPoint(double* val);
        void            gainSchedulerEnable(bool onoff);
        void            gainSchedulerEnable(bool* onoff);
        void 			currentSetPoint(double* val);
		void			instrumentMode(unsigned short* mode);
		void			instrumentMode(unsigned short  mode);
		void			programStatus(unsigned short* status);
		void			programStatus(string* status);
		void			programStatus(unsigned short status);
		void			programStatus(string status);
		void			currentProgram(unsigned short* prog);
		void			currentProgram(unsigned short prog);
		void			currentSegment(unsigned short* segment);
		void			currentSegmentType(unsigned short* type);
		void			currentSegmentType(string* type);
		void			timeSegment(unsigned short* time);
		void			timeProgram(unsigned short* time);
		void			loadProgram(string filename);
		void			deleteProgram(unsigned short prognum);
		void			hasProgram(unsigned short prognum, bool* has);
		void			writeProgram(unsigned short prognum = 0);
		void			controllerVersion(string& version);
		void			resolution(unsigned short* res);
		void			resolution(unsigned short res);
		void			value(unsigned short address, double v);
		void			value(unsigned short address, double* v);
		//bool			showPrograms();
		void			programmer(unsigned short* maxprogs);
		void			maxSegments(unsigned short* maxs);
		enum			states {ALARM1 = 0x01,  ALARM2 = 0x02, ALARM3 = 0x04, ALARM4 = 0x08, MANUAL = 0x10, SENSOR = 0x20, LOOP = 0x40, HEATER = 0x80};
		
	private:
		bool			logic_ops;
		unsigned short	add_crc(unsigned char *z_p, unsigned short z_message_length);
		bool 			check_crc(unsigned char *z_p, unsigned short z_message_length);
		unsigned short 	make_crc(unsigned char *z_p, unsigned short z_message_length);
		void 			statusByte(unsigned char *s);
		unsigned short	addr;
		void 			readWords(unsigned short address, unsigned short numwords, unsigned short* words);
		void			readBits(unsigned short address, unsigned short numbits, unsigned short* bits);
		void			writeWord(unsigned short address, unsigned short word);
		void			writeWords(unsigned short address, unsigned short numwords, unsigned short* words);
		void			writeBit(unsigned short address, unsigned short bitval);
		void			readData(unsigned char* reply, size_t len, unsigned char cmd);
		void			trimProgram(ifstream& infile, vector<string*>& programtxt);
		void			readProgram(unsigned short prognum, unsigned short* data);
		void			makeProgram(vector<string*>& programtxt);
		void			makeSegments(vector<string*>& programtxt);
		void			fillSegments(vector<string*>& programtxt);
		void			checkSequence();
		void			delProgramTxt(vector<string*>& programtxt);
		double			scale;
		unsigned short  maxprogs;
		unsigned short	maxsegs;
		Segment*		getSegment(unsigned short prognum, unsigned short segnum);
		vector<Program*> program;
};
		

#endif
