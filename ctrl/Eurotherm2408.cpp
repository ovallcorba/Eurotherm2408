#include <iostream>
#include <boost/regex.hpp> 
#include <cstring>
#include <ipc_exception.h>
#include <cmath>

extern const char* segtypekeys[];

#include "Eurotherm2408.h"

Eurotherm2408ctrl::Eurotherm2408ctrl(unsigned short devaddr, bool logicOps, TtyDevice* device) : Io(device), logic_ops(logicOps), addr(devaddr)
{
}

Eurotherm2408ctrl::Eurotherm2408ctrl(unsigned short devaddr, bool logicOps, TcpSocket* device) : Io(device), logic_ops(logicOps), addr(devaddr)
{
}


void Eurotherm2408ctrl::init()
{
	unsigned short res;
	consoleDebug(true);
	resolution(&res);
	if(res == 0)
	{
		scale = 10.0;
	}
	else if(res == 1)
	{
		scale = 1.0;
	}
	else
	{
		throw(IpcException("Unknown resolution value", __FILE__, __LINE__));
	}
	programmer(&maxprogs);
	for(int i = 0; i < maxprogs; i++)
	{
		program.push_back((Program* ) 0);
	}
	maxSegments(&maxsegs);
}

/*!

 
	destructor for Eurotherm2408ctrl

	frees any allocated memory 

*/

Eurotherm2408ctrl::~Eurotherm2408ctrl()
{
	vector<Program*>::iterator it;
	for(it = program.begin(); it < program.end(); it++)
	{
		if(*it)
		{
			delete *it;
		}
	}
}

/*!
	reads temperature value

	in - val: address of double where read temperature is stored\n
	
	
	return:\n true: everything ok\n
			false: error, get description with Io::getError() 
*/

void Eurotherm2408ctrl::temperature(double *val)
{
	unsigned short word;
	readWords(0x01, 0x01, &word);
	*val = (double) word / scale;
}


/*!
	writes new setpoint value

	in - val: new setpoint\n
	
	
	return:\n true: everything ok\n
			false: error, get description with Io::getError() 
*/


void Eurotherm2408ctrl::setPoint(double val)
{
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x02, 1, &word);
}


/*!
	reads setpoint value

	in - val: address of double where read setpoint is stored\n
	
	
	return:\n true: everything ok\n
			false: error, get description with Io::getError() 
*/

void Eurotherm2408ctrl::setPoint(double* val)
{
	unsigned short word;
	readWords(0x02, 0x01, &word);
	*val = (double) (word / scale);
	
}

void Eurotherm2408ctrl::setPointLowLimit(double val)
{
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x70, 1, &word);
}

void Eurotherm2408ctrl::setPointHighLimit(double val)
{
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x6f, 1, &word);

}

void Eurotherm2408ctrl::setPointLowLimit(double* val)
{
	unsigned short word;
	readWords(0x70, 0x01, &word);
	*val = (double) (word / scale);
}

void Eurotherm2408ctrl::setPointHighLimit(double* val)
{
	unsigned short word;
	readWords(0x6f, 0x01, &word);
	*val = (double) (word / scale);
}

void Eurotherm2408ctrl::multiplier(double val)
{
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x1805, 1, &word);
}

void Eurotherm2408ctrl::multiplier(double* val)
{
	unsigned short word;
	readWords(0x1805, 0x01, &word);
	*val = (double) (word / scale);
}

/*
 	 Proportional band PID-1
 */

void Eurotherm2408ctrl::proportionalBand_PID1(double val){
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x6, 1, &word);
}

void Eurotherm2408ctrl::proportionalBand_PID1(double* val){
	unsigned short word;
	readWords(0x6, 1, &word);
	*val = (double) (word / scale);
}


/*
 	 Proportional band PID-2
 */

void Eurotherm2408ctrl::proportionalBand_PID2(double val){
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x30, 1, &word);
}

void Eurotherm2408ctrl::proportionalBand_PID2(double* val){
	unsigned short word;
	readWords(0x30, 1, &word);
	*val = (double) (word / scale);
}



/*
 	 Integral time PID-1
 */

void Eurotherm2408ctrl::integralTime_PID1(double val){
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x8, 1, &word);
}

void Eurotherm2408ctrl::integralTime_PID1(double* val){
	unsigned short word;
	readWords(0x8, 1, &word);
	*val = (double) (word / scale);
}

/*
 	 Integral time PID-2
 */

void Eurotherm2408ctrl::integralTime_PID2(double val){
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x31, 1, &word);
}

void Eurotherm2408ctrl::integralTime_PID2(double* val){
	unsigned short word;
	readWords(0x31, 1, &word);
	*val = (double) (word / scale);
}


/*
 	 Derivative time PID-1
 */

void Eurotherm2408ctrl::derivativeTime_PID1(double val){
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x9, 1, &word);
}

void Eurotherm2408ctrl::derivativeTime_PID1(double* val){
	unsigned short word;
	readWords(0x9, 1, &word);
	*val = (double) (word / scale);
}


/*
 	 Derivative time PID-2
 */

void Eurotherm2408ctrl::derivativeTime_PID2(double val){
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x33, 1, &word);
}

void Eurotherm2408ctrl::derivativeTime_PID2(double* val){
	unsigned short word;
	readWords(0x33, 1, &word);
	*val = (double) (word / scale);
}


/*
 	 Cutback high PID-1
 */

void Eurotherm2408ctrl::cutbackHigh(double val){
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x12, 1, &word);
}

void Eurotherm2408ctrl::cutbackHigh(double* val){
	unsigned short word;
	readWords(0x12, 1, &word);
	*val = (double) (word / scale);
}

/*
 	 Cutback low PID-1
 */

void Eurotherm2408ctrl::cutbackLow(double val){
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x11, 1, &word);
}

void Eurotherm2408ctrl::cutbackLow(double* val){
	unsigned short word;
	readWords(0x11, 1, &word);
	*val = (double) (word / scale);

}
/*
 	 Current setpoint
 */


void Eurotherm2408ctrl::currentSetPoint(double* val){
	unsigned short word;
	readWords(0xa0, 1, &word);
	*val = (double) (word / scale);

}
/*
 	 Autotune enable
 */

void Eurotherm2408ctrl::autotune(bool onoff){
	unsigned short word = onoff;
	writeWords(0x10e, 1, &word);

}

void Eurotherm2408ctrl::autotune(bool* onoff){
	unsigned short word;
	readWords(0x10e, 1, &word);
	*onoff = (bool) word;

}

/*
 	 Autotune enable
 */

void Eurotherm2408ctrl::adaptiveTune(bool onoff){
	unsigned short word = onoff;
	writeWords(0x10f, 1, &word);

}

void Eurotherm2408ctrl::adaptiveTune(bool* onoff){
	unsigned short word;
	readWords(0x10f, 1, &word);
	*onoff = (bool) word;

}

/*
 	 Gain scheduler setpoint */

void Eurotherm2408ctrl::gainSchedulerSetPoint(double val){
	unsigned short word = (unsigned short) ::roundl(val * scale);
	writeWords(0x99, 1, &word);
}

void Eurotherm2408ctrl::gainSchedulerSetPoint(double* val){
	unsigned short word;
	readWords(0x99, 1, &word);
	*val = (double) (word / scale);
}

/*
 	 Gain scheduler setpoint */

void Eurotherm2408ctrl::gainSchedulerEnable(bool onoff){
	unsigned short word = onoff;
	writeWords(0x237, 1, &word);
}

void Eurotherm2408ctrl::gainSchedulerEnable(bool* onoff){
	unsigned short word;
	readWords(0x237, 1, &word);
	*onoff = (bool) word;
}





void Eurotherm2408ctrl::value(unsigned short address, double v)
{
	unsigned short word = (unsigned short) ::roundl(v);
	writeWords(address, 1, &word);
}
	
void Eurotherm2408ctrl::value(unsigned short address, double* v)
{
	unsigned short word;
	readWords(address, 0x01, &word);
	*v = (double) (word);
}

/*!
	reads status of running program

	in - status: address of unsigned short to store status\n
	
	
	return:\n true: everything ok\n
			false: error, get description with Io::getError()

	status can be:\n\n

	1  : reset\n
	2  : running\n
	4  : hold\n
	8  : holdback\n
	16 : complete\n
 
*/

void Eurotherm2408ctrl::programStatus(unsigned short* status)
{
	readWords(0x17, 0x01, status);
}

/*!
	reads status of running program

	in - status: address of string to store status\n
	
	
	return:\n true: everything ok\n
			false: error, get description with Io::getError()

	status can be:\n\n

	1  : reset\n
	2  : run\n
	4  : hold\n
	8  : holdback\n
	16 : complete\n
 
*/

void Eurotherm2408ctrl::programStatus(string* status)
{
	unsigned short progstatus;
	programStatus(&progstatus);
	switch(progstatus)
	{
		case 0x1:
			*status = "reset";
		break;
		case 0x2:
			*status = "run";
		break;
		case 0x4:
			*status = "hold";
		break;
		case 0x8:
			*status = "holdback";
		break;
		case 0x10:
			*status = "complete";
		break;
		default:
			*status = "unknown";
			stringstream tmp;
			tmp << "Unknown status value: " << progstatus;
			throw(IpcException(tmp.str(), __FILE__, __LINE__));
		break;
	}
}
/*!
	sets status of current program

	in - status: address of string to store status\n
	
	
	return:\n true: everything ok\n
			false: error, get description with Io::getError()

	status can be:\n\n

	0 : reset ( = stop program)\n
	2 : run   ( = start program)\n
	4 : hold  ( = pause program)\n
 
*/

void Eurotherm2408ctrl::programStatus(unsigned short status)
{
	writeWord(0x17, status);
}

/*!
	sets status of current program

	in - status: address of string to store status\n
	
	
	return:\n true: everything ok\n
			false: error, get description with Io::getError()

	status can be:\n\n

	reset ( = stop program)\n
	run   ( = start program)\n
	hold  ( = pause program)\n
 
*/

void Eurotherm2408ctrl::programStatus(string status)
{
	unsigned short progstatus = 0;
	if(status == "reset")
	{
		progstatus =  0x1;
	}
	else if(status == "run")
	{
		progstatus = 0x2;
	}
	else if(status == "hold")
	{
		progstatus = 0x4;
	}
	if(progstatus)
	{
		programStatus(progstatus);
	}
	else
	{
		stringstream tmp;
		tmp << "Unknown command: " << status;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
}

/*!
	checks status

	in - allowed: bitmask, bits set compared to the read status byte are not considered an error\n
	(cf Eurotherm2408ctrl::states)	
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 
*/

bool Eurotherm2408ctrl::status(string& status_str, unsigned char allowed)
{
	bool b_status = true;
	unsigned char s;
	statusByte(&s);
	stringstream tmp;
	if(!(HEATER & allowed) && (s & HEATER))
	{
		tmp << "Heater failure detected !\n";
		b_status = false;
	}
	else if(!(LOOP & allowed) && (s & LOOP))
	{
		tmp << "Device in open loop !\n";
		b_status = false;
	}
	else if(!(SENSOR & allowed) && (s & SENSOR))
	{
		tmp << "Sensor failure detected !\n";
		b_status = false;
	}
	else if(!(MANUAL & allowed) && (s & MANUAL))
	{
		tmp << "Device in manual mode !\n";
		b_status = false;
	}
	else if(!(ALARM4 & allowed) && (s & ALARM4))
	{
		tmp << "Alarm 4 detected" << endl;
		b_status = false;
	}
	else if(!(ALARM3 & allowed) && (s & ALARM3))
	{
		tmp << "Alarm 3 detected" << endl;
		b_status = false;
	}
	else if(!(ALARM2 & allowed) && (s & ALARM2))
	{
		tmp << "Alarm 2 detected" << endl;
		b_status = false;
	}
	else if(!(ALARM1 & allowed) && (s & ALARM1))
	{
		tmp << "Alarm 1 detected" << endl;
		b_status = false;
	}
	if(!b_status)
	{
		status_str = tmp.str();
	}
	return b_status;
}

/*!
	sets status of manual mode

	in - onoff : bool containing mode
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

	modes can be:\n\n

	0 : manual mode off\n
	1 : manual mode on\n
*/

void Eurotherm2408ctrl::manual(bool onoff)
{
	unsigned short word = onoff;
	writeWords(0x111, 1, &word);
}

/*!
	returns status of manual mode

	in - onoff : address of bool to store mode
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

	modes can be:\n\n

	0 : manual mode off\n
	1 : manual mode on\n
*/

void Eurotherm2408ctrl::manual(bool* onoff)
{
	unsigned short word;
	readWords(0x111, 1,  &word);
	*onoff = (bool) word;
}
	
void Eurotherm2408ctrl::holdBack(bool onoff)
{
	unsigned short word = (unsigned short) ! onoff;
	writeWords(0x116, 1, &word);
}

void Eurotherm2408ctrl::holdBack(bool* onoff)
{
	unsigned short word;
	readWords(0x116, 1, &word);
	*onoff = ! (bool) word;
}

void Eurotherm2408ctrl::highPowerLimit(double l)
{
	unsigned short word = roundl(l * scale); 
	writeWords(0x1e, 1, &word);
}

void Eurotherm2408ctrl::highPowerLimit(double *l)
{
	unsigned short word;
	readWords(0x1e, 1, &word);
	*l = word / scale;
}

void Eurotherm2408ctrl::lowPowerLimit(double l)
{
	unsigned short word = roundl(l * scale);
	writeWords(0x1f, 1, &word);
}

void Eurotherm2408ctrl::lowPowerLimit(double *l)
{
	unsigned short word;
	readWords(0x1f, 1, &word);
	*l = (short) word / scale;
}

void Eurotherm2408ctrl::outputLevel(double *l)
{
	unsigned short word;
	readWords(0x03, 1, &word);
	*l = (short) word / scale;
}

void Eurotherm2408ctrl::outputLevel(double l)
{
	unsigned short word = roundl(l * scale);
	writeWords(0x03, 1, &word);
}

void Eurotherm2408ctrl::outputRateLimit(bool* onoff)
{
	unsigned short word;
	readWords(0x25, 1, &word);
	*onoff = (bool) word;

}

void Eurotherm2408ctrl::outputRateLimit(bool onoff)
{
	unsigned short word = onoff;
	writeWords(0x25, 1, &word);
}

void Eurotherm2408ctrl::statusByte(unsigned char *s)
{
	unsigned char cmd[4] = {0x00, 0x07, 0x0, 0x0};
	cmd[0] = addr & 0x00ff;
	unsigned char reply[64];
	add_crc(cmd, 4);
	SendBin(cmd, 4);
	readData(reply, 5, cmd[0]);
	*s = reply[2];
}

/*!
	returns output status word from controller

	in - word: address of unsigned short int to store word
	(cf Eurotherm2408ctrl::states)	
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

	meaning of bits is:\n\n
	0  : Alarm 1 State (0 = Safe, 1 = Alarm)\n
	1  : Alarm 2 State (0 = Safe, 1 = Alarm)\n
	2  : Alarm 2 State (0 = Safe, 1 = Alarm)\n
	3  : Alarm 2 State (0 = Safe, 1 = Alarm)\n
	4  : Alarm 2 State (0 = Safe, 1 = Alarm)\n
	5  : Alarm 2 State (0 = Safe, 1 = Alarm)\n
	6  : Loop Break    (0 = Good closed loop, 1 = Open Loop)\n
	7  : Heater Fail   (0 = No Fault, 1 = Load fault detected)\n
	8  : Tune Active   (0 = Auto Tune disabled, 1 = Auto Tune active)\n
	9  : Ramp/Program Complete (0 = Running/Reset, 1 = Complete)\n
	10 : PV out of range (0 = PV within table range, 1 = PV out of table range)\n
	11 : DC control module fault (0= Good,. 1= BAD)\n
	12 : Programmer Segment Synchronize (0 = Waiting, 1 = Running)\n
	13 : Remote input sensor break (0 = Good, 1 = Bad)
	14 : IP1 Fault (PV Input)
	15 : Reserved
 
*/

void Eurotherm2408ctrl::outputStatus(unsigned short* word)
{
	readWords(0x4b, 1, word);
}

/*!
	returns instrument mode

	in - mode: address of unsigned short to store mode
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

	modes can be:\n\n

	0 : Normal\n
	1 : Standby\n
	2 : Configuration	
*/

void Eurotherm2408ctrl::instrumentMode(unsigned short* mode)
{
	readWords(0xaf8, 0x1, mode);
}

/*!
	sets instrument mode

	in - mode: unsigned short containing mode value
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

	modes can be:\n\n

	0 : Normal\n
	1 : Standby\n
	2 : Configuration	
*/

void Eurotherm2408ctrl::instrumentMode(unsigned short mode)
{
	if(mode < 0 || mode > 2)
	{
		stringstream tmp;
		tmp << "Invalid mode value: " << mode;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	writeWord(0xaf8, mode);	
}


/*!
	returns current segment, if a program is running

	in - segment: address of unsigned short to store segment
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

	modes can be:\n\n
*/

void Eurotherm2408ctrl::currentSegment(unsigned short* segment)
{
	readWords(0x38, 0x1, segment);
}

/*!
	returns current segment type, if a program is running

	in - segment: address of unsigned short to store segment type
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::currentSegmentType(unsigned short* type)
{
	readWords(0x1d,  0x1, type);
}

/*!
	returns current segment type, if a program is running

	in - type: reference to string to store segment type
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

	segment types are:\n\n

	end\n
	ramp (rate)\n
	ramp (time to target)\n
	dwell\n
	step\n
	call\n
*/

void Eurotherm2408ctrl::currentSegmentType(string* type)
{
	unsigned short segtype;
	readWords(0x1d,  0x1, &segtype);
	switch(segtype)
	{
		case 0x0:
			*type = "end";
		break;
		case 0x1:
			*type = "ramp (rate)";
		break;
		case 0x2:
			*type = "ramp (time to target)";
		break;
		case 0x3:
			*type = "dwell";
		break;
		case 0x4:
			*type = "step";
		break;
		case 0x5:
			*type = "call";
		break;
		default:
			*type = "unknown";
			stringstream tmp;
			tmp << "Unknown segment type: " << segtype;
			throw(IpcException(tmp.str(),  __FILE__, __LINE__));
		break;
	}
}

/*!
	returns remaining run time for the current segment (in 1/10 sec ?)

	in - time : address of unsigned short to store time
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/


void Eurotherm2408ctrl::timeSegment(unsigned short* time)
{
	unsigned short tmp;
	readWords(0x24, 0x1, &tmp);
	*time = tmp / scale;
}

/*!
	returns number of currently running program

	in - time : address of unsigned short to store program number
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::currentProgram(unsigned short* prog)
{
	readWords(0x16, 0x1, prog);
}


/*!
	sets number of program to run

	in - prog : unsigned short containing program number
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::currentProgram(unsigned short prog)
{
	writeWord(0x16, prog);
}

/*!
	returns version of controller software

	in - version : refernce to string to store program number
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::controllerVersion(string& version)
{
	unsigned short verword;
	stringstream   tmp;
	readWords(0x6b, 1, &verword);
	tmp << 'V';
	tmp << (verword >> 8);
	tmp << '.';
	tmp << (verword & 0x00ff);
	tmp >> version;
}

/*!
	returns remaining run time for the current program  (unit still unclear)

	in - time : address of unsigned short to store time
	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::timeProgram(unsigned short* time)
{
	readWords(0x3a, 0x1, time);
}

/*!
	returns reolution configuration

	in - res : address of unsigned short to store resolution

	values:\n
	0 : full resolution\n
	1 : integer resolution\n

	
	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::resolution(unsigned short* res)
{
	readWords(0x3106, 0x1, res); //2804
}

void Eurotherm2408ctrl::resolution(unsigned short res)
{
	unsigned short word = res;
	writeWords(0x2ff3, 1, &word);
}


void Eurotherm2408ctrl::trimProgram(ifstream& infile, vector<string*>& programtxt)
{
	char inbuf[1024];
	char *rptr = inbuf;
	memset(inbuf, 0, 1024);
	while(infile.getline(inbuf, 1024))
	{
		string line("");
		for(rptr = inbuf;*rptr;rptr++)
		{
			if(*rptr == '#')
			{
				break;
			}
			else
			{
				line += *rptr;
			}
		}
		boost::regex expr("\\s+");
		line = boost::regex_replace(line, expr, " ");
		expr = "^ ";
		line = boost::regex_replace(line, expr, "");
		expr = " $";
		line = boost::regex_replace(line, expr, "");
		string::iterator it;
  		for (it=line.begin() ; it < line.end(); it++)
		{
			*it = toupper(*it);
		}
		if(line.size() > 0)
		{
			programtxt.push_back(new string(line));
		}
		memset(inbuf, 0, 1024);
	}
}

/*!
	loads (and parses) program 

	in - filename : string containing filename of file with program

	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::loadProgram(string filename)
{
	// start with a clean state
	vector<Program*>::iterator it;
	vector<string*> programtxt;
	for(it = program.begin(); it < program.end(); it++)
	{
		if(*it)
		{
			delete *it;
			*it = (Program*) 0;
		}
	}
	ifstream infile;
	infile.open(filename.c_str());
	if(!infile.is_open())
	{
		stringstream tmp;
		tmp << "Error opening " << filename;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	try
	{
		trimProgram(infile, programtxt);
		makeProgram(programtxt);
		makeSegments(programtxt);
		fillSegments(programtxt);
		checkSequence();
	}
	catch(IpcException& e)
	{
		delProgramTxt(programtxt);
		throw(e);
	}
	delProgramTxt(programtxt);
}

/*!
	deletes allocated strings in vector programtxt
	
	in - programtxt: vector of pointers to string

	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::delProgramTxt(vector<string*>& programtxt)
{
	vector<string*>::iterator its;
	for(its = programtxt.begin(); its < programtxt.end(); its++)
	{
		if(*its)
		{
			delete *its;
		}
	}
}

/*!
	checks for gaps in the segment numbering

	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::checkSequence()
{
	vector<Program*>::iterator it;
	for(it = program.begin(); it < program.end(); it++)
	{
		if(*it) // is there a program ?
		{
			vector<Segment*>::iterator its;
			for(its = (*it)->segments.begin() + 1; its < (*it)->segments.end();its++)
			{
				if(*its && !*(its - 1)) // is there a valid segment in front of this one ?
				{
					stringstream tmp;
					tmp  << "Missing segment " << (*its)->segNumber() - 1 
							<<  " in program " << (*it)->progNumber();
					throw(IpcException(tmp.str(),  __FILE__, __LINE__));
				}
			}
		}
	}
}

void Eurotherm2408ctrl::makeProgram(vector<string*>& programtxt)
{
	vector<string*>::iterator it;
	string* line;
	stringstream errtmp;
	for(it = programtxt.begin(); it < programtxt.end(); it++)
	{
		if(!*it)
		{
			throw(IpcException("Invalid line in program" ,  __FILE__, __LINE__));
		}
		unsigned short  prognum, ht, ru, du, pc;
		double hv;
		char typestr[16];
		line = *it;
		if(line->find("PROG:") != string::npos)
		{
			const char* keys[] = {"HBTYPE:", "HBVALUE:", "RUNITS:", "DWUNITS:", "CYCLES:", "PROG:", 0};
			for(int i = 0; keys[i]; i++)
			{
				size_t start 	= line->find(keys[i]);
				if(start == string::npos)
				{
					errtmp << "Error parsing " << line;
					throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
				}
				size_t end		= line->find_first_of(" ", start);
				string tmp		= line->substr(start, end - start);
				switch(i)
				{
					case 0:
						if(sscanf(tmp.c_str(), "HBTYPE:%s", typestr) != 1)
						{
							errtmp << "Error reading holdback type " << " in " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
						if(!Program::hbtype(typestr, &ht))
						{
							errtmp << "Unknown holdback type " << typestr << " in " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					break;
					case 1:
						hv = toDouble(strchr(tmp.c_str(), ':') + 1);
					break;
					case 2:
						if(sscanf(tmp.c_str(), "RUNITS:%s", typestr) != 1)
						{
							errtmp  << "Error reading ramp units in " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
						if(!Program::tunits(typestr, &ru))
						{
							errtmp << "Unknown time unit " << typestr << " in " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					break;
					case 3:
						if(sscanf(tmp.c_str(), "DWUNITS:%s", typestr) != 1)
						{		
							errtmp << "Error reading dwell units in " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
						if(!Program::tunits(typestr, &du))
						{
							errtmp << "Unknown time unit " << typestr << " in " << line << endl;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					break;
					case 4:
						pc = toDouble(strchr(tmp.c_str(), ':') + 1);
					break;
					case 5:
						prognum = toLong(strchr(tmp.c_str(), ':') + 1);
					break;
				}
			}
			Program* tprog = new Program(maxsegs, prognum, ht, ::roundl(hv * scale), ru, du, pc);
			if(program.at(prognum -1))
			{
				errtmp << "Program " << prognum << " already exists";
				throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
			}
			program.at(prognum -1) = tprog;
		}
	}
}

void Eurotherm2408ctrl::makeSegments(vector<string*>& programtxt)
{
	vector<string*>::iterator it;
	string* line;
	stringstream errtmp;
	for(it = programtxt.begin(); it < programtxt.end(); it++)
	{
		if(!*it)
		{
			throw(IpcException("Invalid line in program", __FILE__, __LINE__));
		}
		
		unsigned short segnum, prognum, st; 
		string tmp;
		line = *it;
		st = 0xffff;
		if(line->find("SEG") != string::npos)
		{
			for(int i = 0; segtypekeys[i]; i++)
			{
				size_t start 	= line->find(segtypekeys[i]);
				if(start != string::npos) // key is in
				{
					size_t end		= line->find_first_of(" ", start);
					tmp				= line->substr(start, end - start);
				}
				else
				{
					continue;
				}
				switch(i)
				{
					case 0:
						if(sscanf(tmp.c_str(), "SEG:%hi:%hi", &prognum, &segnum) != 2)
						{
							errtmp << "Error reading program/segment number in " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
						if(segnum > maxsegs)
						{
							errtmp << "Invalid segment number " << segnum  << ", max. " << maxsegs;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
						if(!program.at(prognum -1))
						{
							errtmp<< "No program for " << line << endl;
				 			throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					break;
					default:
						if(!program.at(prognum -1)->segType(tmp, &st))
						{
							errtmp << "Unknown segment type in: " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					break;
				} 
			}
			if(st == 0xffff)
			{
				errtmp << "Unknown segment type in: " << line;
				throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
			}
			Segment* tseg = new Segment(segnum, prognum, st);
			program.at(tseg->progNumber() -1)->addSegment(tseg);
		}
	}
}

void Eurotherm2408ctrl::fillSegments(vector<string*>& programtxt)
{
	vector<string*>::iterator it;
	string* line;
	stringstream errtmp;
	for(it = programtxt.begin(); it < programtxt.end(); it++)
	{
		if(!*it)
		{
			errtmp << "Invalid line in program\n";
			throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
		}
		line = *it;
		const char* akeys[] = {"LOPS:", "ENDTYPE:", 0};
		const char* bkeys[] = {"LOPS:", "SETPOINT:", "RATE:", 0};
		const char* ckeys[] = {"LOPS:", "SETPOINT:", "DURATION", 0};
		const char* dkeys[] = {"LOPS:", "DURATION:", 0};
		const char* ekeys[] = {"LOPS:", "SETPOINT:", 0};
		const char* fkeys[]	= {"PROGRAM:", "CYCLES:", 0};
		unsigned short st;
		Segment* tseg = 0;
		if(line->find("SEG") != string::npos)
		{
			string tmp;
			unsigned short segnum, prognum;
			size_t start 	= line->find("SEG");
			size_t end		= line->find_first_of(" ", start);
			tmp				= line->substr(start, end - start);
			if(sscanf(tmp.c_str(), "SEG:%hi:%hi", &prognum, &segnum) != 2)
			{
				errtmp << "Error reading program/segment number in " << line ;
				throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
			}
			for(int i = 0; segtypekeys[i]; i++)
			{
				size_t start 	= line->find(segtypekeys[i]);
				if(start == string::npos)
				{
					continue;
				}
				size_t end		= line->find_first_of(" ", start);
				tmp				= line->substr(start, end - start);
				
			}
			program.at(prognum -1)->segType(tmp, &st);
			switch(st)
			{
				case Segment::SEND:
					unsigned short lops, et;
					lops = et = 0xffff;
					
					for(int i = 0; akeys[i]; i++)
					{
						size_t start 	= line->find(akeys[i]);
						if(start == string::npos)
						{
							continue;
						}
						size_t end		= line->find_first_of(" ", start);
						tmp				= line->substr(start, end - start);
						switch(i)
						{
							case 0:
								lops = toLong(strchr(tmp.c_str(), ':') + 1);
							break;
							case 1:
								et = toLong(strchr(tmp.c_str(), ':') + 1);
							break;
						}
					}
					if(logic_ops)
					{
						if(et == 0xffff || lops == 0xffff)
						{
							errtmp << "Error parsing " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					}
					else
					{
						if(et == 0xffff) 
						{
							errtmp << "Error parsing " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					}
					tseg = getSegment(prognum, segnum);
					if(!tseg)
					{
						errtmp << "No segment with number " << segnum << " in program " << prognum;
						throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
					}
					tseg->endType(et);
					if(logic_ops)
					{
						tseg->logicOps(lops);
					}
				break;
				case Segment::SRRATE:
				{
					unsigned short ra, lops;
					double sp = -1.0;
					ra = lops = 0xffff;
					for(int i = 0; bkeys[i]; i++)
					{
						size_t start 	= line->find(bkeys[i]);
						if(start == string::npos)
						{
							continue;
						}
						size_t end		= line->find_first_of(" ", start);
						tmp				= line->substr(start, end - start);
						switch(i)
						{
							case 0:
								lops = toLong(strchr(tmp.c_str(), ':') + 1);
							break;
							case 1:
								sp = toDouble(strchr(tmp.c_str(), ':') + 1);
							break;
							case 2:
								ra = toLong(strchr(tmp.c_str(), ':') + 1);
							break;
						}
					}
					if(logic_ops)
					{
				 		if(ra == 0xffff || lops == 0xffff || sp == -1.0)
						{
							errtmp << "Error parsing " << line ;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					}
					else
					{
						if(ra == 0xffff || sp == -1.0)
						{
							errtmp << "Error parsing " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					}
					tseg = getSegment(prognum, segnum);
					if(!tseg)
					{
						errtmp << "No segment with number " << segnum << " in program " << prognum << endl;
						throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
					}
					tseg->rate(ra * scale);
					tseg->setPoint((unsigned short) (::roundl(sp * scale)));
					if(logic_ops)
					{
						tseg->logicOps(lops);
					}
				break;
				}
				case Segment::STRATE:
				{
					unsigned short dur, lops;
					double sp;
					dur = lops = 0xffff;
					sp  = -1.0;
					for(int i = 0; ckeys[i]; i++)
					{
						size_t start 	= line->find(ckeys[i]);
						if(start == string::npos)
						{
							continue;
						}
						size_t end		= line->find_first_of(" ", start);
						tmp				= line->substr(start, end - start);
						switch(i)
						{
							case 0:
								lops = toLong(strchr(tmp.c_str(), ':') + 1);
							break;
							case 1:
								sp = toDouble(strchr(tmp.c_str(), ':') + 1);
							break;
							case 2:
								dur = toLong(strchr(tmp.c_str(), ':') + 1);
							break;
						}
					}
					if(logic_ops)
					{
				 		if(dur == 0xffff || lops == 0xffff || sp == -1.0)
						{
							errtmp << "Error parsing " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					}
					else
					{
						if(dur == 0xffff || sp == -1.0)
						{
							errtmp << "Error parsing " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					}
					tseg = getSegment(prognum, segnum);
					tseg->duration(dur * scale);
					tseg->setPoint((unsigned short) (::roundl(sp * scale)));
					if(logic_ops)
					{
						tseg->logicOps(lops);
					}
				}
				break;
				case Segment::SDWELL:
				{
					unsigned short dur, lops;
					dur = lops = 0xffff;
					for(int i = 0; dkeys[i]; i++)
					{
						size_t start 	= line->find(dkeys[i]);
						if(start == string::npos)
						{
							continue;
						}
						size_t end		= line->find_first_of(" ", start);
						tmp				= line->substr(start, end - start);
						switch(i)
						{
							case 0:
								lops = toLong(strchr(tmp.c_str(), ':') + 1);
							break;
							case 1:
								dur = toLong(strchr(tmp.c_str(), ':') + 1);
							break;
						}
					}
					if(logic_ops)
					{
						if(dur == 0xffff || lops == 0xffff)
						{
							errtmp << "Error parsing " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					}
					else
					{
						if(dur == 0xffff)
						{
							errtmp << "Error parsing " << line << endl;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					}
					tseg = getSegment(prognum, segnum);
					if(!tseg)
					{
						errtmp << "No segment with number " << segnum << " in program " << prognum ;
						throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
					}
					tseg->duration(dur * scale);
					if(logic_ops)
					{
						tseg->logicOps(lops);
					}
				}
				break;
				case Segment::SSTEP:
				{
					unsigned short lops = 0xffff;
					double sp			= -1.0;
					for(int i = 0; ekeys[i]; i++)
					{
						size_t start 	= line->find(ekeys[i]);
						if(start == string::npos)
						{
							continue;
						}
						size_t end		= line->find_first_of(" ", start);
						tmp				= line->substr(start, end - start);
						switch(i)
						{
							case 0:
								lops = toLong(strchr(tmp.c_str(), ':') + 1);
							break;
							case 1:
								sp = toDouble(strchr(tmp.c_str(), ':') + 1);
							break;
						}
					}
					if(logic_ops)
					{
						if(lops == 0xffff || sp == -1.0)
						{
							errtmp << "Error parsing " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					}
					else
					{
						if(sp == -1.0)
						{
							errtmp << "Error parsing " << line;
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
						}
					}
					tseg = getSegment(prognum, segnum);
					if(!tseg)
					{
						errtmp << "No segment with number " << segnum << " in program " << prognum;
						throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
					}
					tseg->setPoint((unsigned short) (::roundl(sp * scale)));
					if(logic_ops)
					{
						tseg->logicOps(lops);
					}
				}
				break;
				case Segment::SCALL:
				{
					if(maxprogs < 2)
					{
							errtmp << "Segment type " << tmp << " not supported by this hardware";
							throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
					}
					unsigned short pn, cc;
					pn = cc = 0xffff;
					for(int i = 0; fkeys[i]; i++)
					{
						size_t start 	= line->find(fkeys[i]);
						if(start == string::npos)
						{
							continue;
						}
						size_t end		= line->find_first_of(" ", start);
						tmp				= line->substr(start, end - start);
						switch(i)
						{
							case 0:
								toLong(strchr(tmp.c_str(), ':') + 1);
							break;
							case 1:
								cc = toLong(strchr(tmp.c_str(), ':') + 1);
							break;
						}
					}
					if(pn == 0xffff || cc == 0xffff)
					{
						errtmp << "Error parsing " << line << endl;
						throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
					}
					tseg = getSegment(prognum, segnum);
					if(!tseg)
					{
						errtmp << "No segment with number " << segnum << " in program " << prognum;
						throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
					}
					tseg->programNumber(pn);
					tseg->callCycles(cc);
				}
			}
		}
	}
}

Segment* Eurotherm2408ctrl::getSegment(unsigned short prognum, unsigned short segnum)
{
	vector<Program*>::iterator it;
	for(it =  program.begin() ; it < program.end(); it++)
	{
		if(*it)
		{
			if((*it)->progNumber() == prognum)
			{
				vector<Segment*>::iterator its;
				for(its = (*it)->segments.begin(); its < (*it)->segments.end();its++)
				if(*its)
				{
					if((*its)->segNumber() == segnum)
					{
						return *its;
					}
				}
			}
		}
	}
	return (Segment*) 0;			
}

/*!
	returns programmer type, i.e. max. number of programs which can be stored\n
	in the controller

	in - maxprogs : address of unsigned short to store max. number of programs

	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/
void Eurotherm2408ctrl::programmer(unsigned short* maxprogs)
{
	readWords(0x205, 1, maxprogs);
}

void Eurotherm2408ctrl::maxSegments(unsigned short* maxs)
{
	readWords(0xd3, 1, maxs);
}

/*!
	uploads program loaded with  Eurotherm2408ctrl::loadProgram() to\n
	the controller

	in - prognum : number of program to upload, if 0 (the default) all programs
				   will be uploaded

	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::writeProgram(unsigned short prognum)
{
	unsigned short maxprogs;
	stringstream errtmp;
	programmer(&maxprogs);
	if(prognum > maxprogs)
	{
		errtmp << "Invalid programm number " << prognum << ". max: " << maxprogs;
		throw(IpcException(errtmp.str(),  __FILE__, __LINE__));	
	}
	
	vector<Program*>::iterator it;
	for(it =  program.begin() ; it < program.end(); it++)
	{
		if(*it)
		{
			if((*it)->progNumber() == prognum || prognum == 0)
			{
				writeWord((*it)->progAddress(), (*it)->holdBackType());
				writeWord((*it)->progAddress() + 1, (*it)->holdBackValue());
				writeWord((*it)->progAddress() + 2, (*it)->rampUnits());
				writeWord((*it)->progAddress() + 3, (*it)->dwellUnits());
				writeWord((*it)->progAddress() + 4, (*it)->cycles());
				vector<Segment*>::iterator its;
				for(its = (*it)->segments.begin(); its < (*it)->segments.end();its++)
				{
					if(*its)
					{
						writeWord((*its)->segAddress(), (*its)->segmentType());
						switch((*its)->segmentType())
						{
							case Segment::SEND:
								unsigned short et, lop;
								/*if(!(*its)->endPower(&ep))
									return false;*/
								(*its)->endType(&et);
								(*its)->logicOps(&lop);
								writeWord((*its)->segAddress() + 3, et);
								/*cerr << "Address: " << dec << (*its)->segAddress() + 1 << " Value " << ep << endl;
								writeWord((*its)->segAddress() + 1, ep);*/
								if(logic_ops)
								{
									writeWord((*its)->segAddress() + 4, lop);
								}	
							break;
							case Segment::SRRATE:
								unsigned short sp, ra;
								
								(*its)->setPoint(&sp);
								(*its)->rate(&ra);
								(*its)->logicOps(&lop);
								writeWord((*its)->segAddress() + 1, sp);
								writeWord((*its)->segAddress() + 2, ra);
								if(logic_ops)
								{
									writeWord((*its)->segAddress() + 4, lop);
								}
							break;
							case Segment::STRATE:
								unsigned short dur;
								(*its)->setPoint(&sp);
								(*its)->duration(&dur);
								(*its)->logicOps(&lop);
								writeWord((*its)->segAddress() + 1, sp);
								writeWord((*its)->segAddress() + 2, dur);
								if(logic_ops)
								{
									writeWord((*its)->segAddress() + 4, lop);
								}
							break;
							case Segment::SDWELL:
								(*its)->duration(&dur);
								(*its)->logicOps(&lop);
								writeWord((*its)->segAddress() + 2, dur);
								if(logic_ops)
								{
									writeWord((*its)->segAddress() + 4, lop);
								}
							break;
							case Segment::SSTEP:
								(*its)->setPoint(&sp);
								(*its)->logicOps(&lop);
								writeWord((*its)->segAddress() + 1, sp);
								if(logic_ops)
								{
									writeWord((*its)->segAddress() + 4, lop);
								}
							break;
							case Segment::SCALL:
								unsigned short pn, cy;
								(*its)->programNumber(&pn);
								(*its)->callCycles(&cy);
								writeWord((*its)->segAddress() + 3, pn);
								writeWord((*its)->segAddress() + 4, cy);
							break;
							default:
								errtmp << "Unknow segment type " << (*its)->segmentType() << endl;
								throw(IpcException(errtmp.str(),  __FILE__, __LINE__));
							break;		
						}
							
					}
				}
			}
		}
	}
}

/*!
	removes program prognum from the controller

	in - prognum : number of program to delete

	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::deleteProgram(unsigned short prognum)
{
	unsigned short progstart = 0x2000 + prognum * PROGSIZE * 2;
	writeWord(progstart + 4, 0);
	progstart += SEGSIZE;
	unsigned short data = 0;
	for(int i = 0; i < maxsegs + 1; i++)
	{
		writeWords(progstart, 1, &data);
		progstart +=  SEGSIZE;
	}
}

/*bool Eurotherm2408ctrl::showPrograms()
{
	vector<Program*>::iterator it;
	unsigned short* data;
	for(it = program.begin() ; it < program.end(); it++)
	{
		if(*it)
		{
			if((*it)->progNumber() == 5)
			{
				data = (*it)->data();
				for(int i = 1; i < SEGSIZE + 1; i++)	
				{
					cerr << hex << showbase << "<" << data[i - 1] << ">";
				}
				cerr << endl;
				vector<Segment*>::iterator its;
				for(its = (*it)->segments.begin(); its < (*it)->segments.end();its++)
				{
					if(*its)
					{
						unsigned short* d = (*its)->data();
						int i;
						for(i = 0; i < SEGSIZE; i++)
						{
								cerr << hex << showbase << "<" << d[i] << ">";
						}
						cerr << endl;		
					}
				}
			}
		}
	}
	return true;
}*/

/*!
	gets program data from controller memory

	in - prognum : number of program to read

	in - data: address of  unsigned short[PROGSIZE] to store program data

*/

void Eurotherm2408ctrl::readProgram(unsigned short prognum, unsigned short* data)
{
	unsigned short progstart = 0x2000 + prognum * PROGSIZE * sizeof(unsigned short);
	readWords(progstart, PROGSIZE * sizeof(unsigned short), data);
}

/*!
	checks whether a program prognum exists in the controller

	in - prognum : number of program  to check

	in - has :  address of bool to store existence/non-existence

	has = true : program exists at prognum
	has = false: no program exists at prognum

	return:\n true: everything ok\n
	false: something is fishy, get description with Io::getError() 

*/

void Eurotherm2408ctrl::hasProgram(unsigned short prognum, bool* has)
{
	*has = false; 
	unsigned short data[PROGSIZE * 2];
	readProgram(prognum, data);
	for(int i = 1; i < PROGSIZE * 2 + 1; i++)
	{
		if(data[i -1] != 0x8000 && data[i -1] != 0)
		{
			*has = true;
		}
	}
}	

void Eurotherm2408ctrl::readWords(unsigned short address, unsigned short numwords, unsigned short* words)
{
	unsigned char cmd[8];
	unsigned char* reply  = new unsigned char[5 + numwords * sizeof(unsigned short)];
	cmd[0] = addr & 0x00ff;
	cmd[1] = 0x03;
	
	unsigned short blocks = numwords / MAXREAD;
	unsigned short remain = numwords % MAXREAD;
	unsigned short wordstoread;
	unsigned short addresstoread;
	do
	{
		if(blocks > 0)
		{
			addresstoread 	= htons(address);
			wordstoread 	= MAXREAD;
			address			+= MAXREAD;
			--blocks;
		}
		else if(remain > 0)
		{
			addresstoread 	= htons(address);
			wordstoread 	= remain;
			remain			= 0;
		}
		numwords = htons(wordstoread);
		memcpy(&cmd[2], &addresstoread, sizeof(unsigned short));
		memcpy(&cmd[4], &numwords, sizeof(unsigned short));
		add_crc(cmd, 8);
		SendBin(cmd, 8);
		readData(reply, 5 + ntohs(numwords) * sizeof(unsigned short), cmd[1]);
		memcpy(words, &reply[3], reply[2]);
	} while (remain > 0 || blocks > 0);

	//swap data
	for(unsigned char i = 0; i < reply[2] / sizeof(unsigned short); i++)
	{
		words[i] = ntohs(words[i]);
	}
	delete[] reply;
}

void Eurotherm2408ctrl::readBits(unsigned short address, unsigned short numbits, unsigned short* bits)
{
	if(numbits > 8 * sizeof(unsigned short))
	{
		throw(IpcException("Cannot read more than 16 bits",  __FILE__, __LINE__));
	}
	unsigned char cmd[8]; 
	unsigned char* reply  = new unsigned char[5 + (numbits / sizeof(char) + 1)];
	cmd[0] = addr & 0x00ff;
	cmd[1] = 0x02;
	address = htons(address);
	numbits = htons(numbits);
	memcpy(&cmd[2], &address, sizeof(unsigned short));
	memcpy(&cmd[4], &numbits, sizeof(unsigned short));
	add_crc(cmd, 8);
	SendBin(cmd, 8);
	readData(reply, 5 + (ntohs(numbits) / 8 + 1), cmd[1]);
	memcpy(bits, &reply[3], reply[2]);
	delete[] reply;
}

void Eurotherm2408ctrl::writeBit(unsigned short address, unsigned short bitval)
{
	unsigned char cmd[8];
	unsigned char reply[8];
	cmd[0] = addr & 0x00ff;
	cmd[1] = 0x05;
	address = htons(address);
	bitval =  ntohs(bitval);
	memcpy(&cmd[2], &address, sizeof(unsigned short));
	memcpy(&cmd[4], &bitval, sizeof(unsigned short));
	add_crc(cmd, 8);
	SendBin(cmd, 8);
	readData(reply, 8, cmd[1]);
}

void Eurotherm2408ctrl::readData(unsigned char* reply, size_t len, unsigned char cmd)
{
	// first check whether we got an error, error responses always have 5 bytes length
	try
	{
		readn(reply, 5, true);
	}
	catch(IpcException& e)
	{
		unlockMutex();
		throw(e);
	}
	// this indicates an error
	if( reply[1] == (cmd | 0x80))
	{
		if(!check_crc(reply, 5))
		{
			unlockMutex();
			throw(IpcException("CRC error",  __FILE__, __LINE__));
		}
		stringstream tmp("Device error: ");
		if(reply[2] == 0x2)
		{
			tmp << "illegal data address";
		}
		else if(reply[2] == 0x3)
		{
			tmp << "illegal data value";
		}
		else
		{
			tmp << "unknown error code";
		}
		unlockMutex();
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	readn(reply + 5, len - 5, true);
	if(!check_crc(reply, len))
	{
		throw(IpcException("CRC error",  __FILE__, __LINE__));
	}
}
	

void Eurotherm2408ctrl::writeWord(unsigned short address, unsigned short word)
{
	unsigned char cmd[8], reply[8];
	cmd[0] 	= addr & 0x00ff;
	cmd[1] 	= 0x06;
	address = htons(address);
	word 	= htons(word);
	memcpy(&cmd[2], &address, sizeof(unsigned short));
	memcpy(&cmd[4], &word, sizeof(unsigned short));
	add_crc(cmd, 8);
	SendBin(cmd, 8);
	readData(reply, 8, cmd[1]);
}

void Eurotherm2408ctrl::writeWords(unsigned short address, unsigned short numwords, unsigned short* words)
{
	if(numwords > 125)
	{
		throw(IpcException("Can't write more than 125 words",  __FILE__, __LINE__));
	}
	unsigned char* cmd = new unsigned char[9 + numwords * sizeof(unsigned short)];
	unsigned char reply[8];
	unsigned char* rptr = (unsigned char*) words;
	unsigned short bytes = numwords * sizeof(unsigned short);
	int i;

	cmd[0] 	= addr & 0x00ff;
	cmd[1] 	= 0x10;
	address = htons(address);
	numwords = htons(numwords);
	memcpy(&cmd[2], &address, sizeof(unsigned short));
	memcpy(&cmd[4], &numwords, sizeof(unsigned short));
	cmd[6] = bytes;
	// set values to network byte order
	for(i = 0; i < ntohs(numwords); i++)
	{
		words[i] = htons(words[i]);
	}
	// copy words to write buffer 
	for(i = 0; i < bytes; i++)
	{
		cmd[7 + i] = rptr[i];
	}
	add_crc(cmd, 9 + bytes);
	SendBin(cmd, 9 + bytes);
	try
	{
		readData(reply, 8, cmd[1]);
	}
	catch(IpcException& e)
	{
		delete[] cmd;
		throw(e);
	}
	delete[] cmd;
	
}

unsigned short Eurotherm2408ctrl::add_crc(unsigned char *z_p, unsigned short z_message_length)
{
	unsigned short crc;
	z_message_length -=2;
	crc = make_crc(z_p, z_message_length);
	::memcpy(z_p + z_message_length, &crc, 2);
	return crc;
}

bool  Eurotherm2408ctrl::check_crc(unsigned char *z_p, unsigned short z_message_length)
{
	unsigned short crc;
	z_message_length -=2;
	crc = make_crc(z_p, z_message_length);
	if((crc >> 8) == *(z_p + z_message_length + 1) && (crc % 256) == *(z_p + z_message_length) )
	{
		return true;
	}
	return false;
}

unsigned short Eurotherm2408ctrl::make_crc(unsigned char *z_p, unsigned short z_message_length)
{
	unsigned short CRC= 0xffff;
	unsigned short next;
	unsigned short carry;
	unsigned short n;
	while (z_message_length--)
	{
		next = (unsigned short)	*z_p;
		CRC ^= next;
		for (n = 0; n < 8; n++)
		{
			carry = CRC & 0x01;
			CRC >>= 1;
			if (carry)
			{
				CRC ^= 0xA001;
			}
		}
		z_p++;
	}
	return CRC;
}
