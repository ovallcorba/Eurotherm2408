#ifndef EUROTHERMPROGRAM_H
#define EUROTHERMPROGRAM_H

#include <vector>

using namespace std;

#define PROGSTART 0x2000
#define PROGSIZE 68
#define SEGSIZE  8
#define MAXREAD  125 // max. number of words which can be read in one go


/*! Class representing a segment of a program for the Eurotherm 2408 Temperature Controller.\n 
	Segemnts are stored in the controller as words (unsigned short) written to\n
	specific memory addresses.\n\n

	A program can contain up to 16 segments depending on the hardware you bought.

	The class is basically a wrapper around an unsigned short[8] array containing the\n
	segment data and provides read/write methods doing necessary error checking and some convenience\n
	methods.


*/

//Segment::vector<SegmentType*> segTypes;

class Segment
{
	public:
		Segment();
		Segment(unsigned short pn, unsigned short sn, unsigned short st); 
		~Segment(){}
		/*! sets error handling object for this segment\n\n 
			in : g : pointer to error handling object */ 
		/*! returns segment type of this segment\n
			for possible types cf. enum Segment::segTypes */
		unsigned short	segmentType() {return segData[0];}
		void			segmentType(unsigned short type);
		enum			segTypes { SEND, SRRATE, STRATE, SDWELL, SSTEP, SCALL};
		/*! returns number of program this segment belongs to */
		unsigned short	progNumber() {return progNum;}
		/*! sets number of program this segment belongs to */
		void			progNumber(unsigned short pn) {progNum = pn;}
		/*! gets segment number of this segemnt */
		unsigned short	segNumber()	{return segNum;}
		void			segNumber(unsigned short sn);
		void			setPoint(unsigned short* sp);
		void			setPoint(unsigned short sp);
		/*bool			endPower(unsigned short* ep);
		bool			endPower(unsigned short ep);*/
		void			duration(unsigned short *du);
		void			duration(unsigned short du);
		void			rate(unsigned short *ra);
		void			rate(unsigned short ra);
		void			programNumber(unsigned short *pn);
		void			programNumber(unsigned short pn);
		void			endType(unsigned short *et);
		void			endType(unsigned short et);
		void			callCycles(unsigned short *cc);
		void			callCycles(unsigned short cc);
		void			logicOps(unsigned short* op);
		void			logicOps(unsigned short op);
		/*! returns status of segment\n\n true = segment ok\n false = something is wrong\n
			get error description with Io::getError() */
		bool			ok() {return segOk;}
		/*! returns pointer to segment data (unsigned short[SEGSIZE]) */
		unsigned short*  data() { return segData;}
		/*! returns modbus storage address for this segment */
		unsigned short segAddress() {return PROGSTART + PROGSIZE * sizeof(unsigned short) * progNum + segNum * SEGSIZE;}
	private:
		unsigned short segData[SEGSIZE];
		unsigned short	progNum;
		unsigned short	segNum;
		void			validSegment();
		bool			segOk;
		
};


/*! Class representing a program  for the Eurotherm 2408 Temperature Controller.\n 
	Programs are stored in the controller as words (unsigned short) written to\n
	specific memory addresses.\n\n

	A controller can store 0, 1, 4 or 20 programs depending on the hardware you bought.\n
	(find out about this with Eurotherm2408ctrl::programmer())

	A program can contain up to 16 segments which constitute what the program does.\n
	These are represented with class Segment::Segment().

	The class is basically a wrapper around an unsigned short[8] array containing the\n
	program data and provides read/write methods doing necessary error checking and some convenience\n
	methods. Besides, it contains segment objects describing the segments going with a program.


*/
class Program 
{
	public:
		Program(unsigned short maxsegs);
		Program(unsigned short maxsegs, unsigned short pn, unsigned short ht, unsigned short hv, unsigned short ru, unsigned short du, unsigned short pc);
		~Program();
		void 			holdBackType(unsigned short type);
		bool 			holdBackValue(unsigned short hv);
		void 			rampUnits(unsigned short unit);
		void 			dwellUnits(unsigned short unit);
		/*!	gets holdback type for this program\n
			return: \n holdback type */
		unsigned short	holdBackType() {return genData[0];}
		unsigned short holdBackValue(); 
		/*!	gets ramp units for this program\n
			return: \n ramp units */
		unsigned short	rampUnits() {return genData[2];}
		/*!	gets dwell units for this program\n
			return: \n ramp units */
		unsigned short	dwellUnits() {return genData[3];}
		void 			cycles(unsigned short cycle) {genData[4] = cycle;}
		/*!	gets program cycles for this program\n
			return: \n program cycles */
		unsigned short	cycles() {return genData[4];}
		/*! sets error handling object for this program\n\n 
			in : g : pointer to error handling object */
		enum			holdBackType 	{ HBNONE, HBLOW, HBHIGH, HBBAND};
		enum			rdUnits			{ RDSEC, RDMIN, RDHOURS};
		/*!	gets program number for this program\n
			return: \n program number */
		unsigned short	progNumber() {return progNum;}
		/*! returns status of program\n\n true = program ok\n false = something is wrong\n
			get error description with Io::getError() */
		bool			ok()		 {return progOk;}
		void			addSegment(Segment* seg);
		static bool		tunits(const char* ttype, unsigned short* ntype);
		static bool		hbtype(const char* htype, unsigned short* ntype);
		/*!	gets modbus storage address for this program */
		unsigned short	progAddress() { return PROGSTART + PROGSIZE * sizeof(unsigned short) * progNum;}
		vector<Segment*> segments;
		/*!	gets program data (unsigned short[SEGSIZE]) for this program */
		unsigned short* data() { return genData; }
		bool segType(string stype, unsigned short* ntype);

		
	private:
		unsigned short 			genData[SEGSIZE];
		unsigned short			progNum;
		bool					progOk;
		};

#endif
