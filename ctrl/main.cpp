#include <iostream>

#include "Eurotherm2408.h"

extern "C"
{
#include <stdlib.h>
}

int main(int argc, char** argv)
{
	Eurotherm2408ctrl* eut = new Eurotherm2408ctrl(1, false, new ClientSocket("haspp07ds003", "10002", 1));
	if(!eut->ok())
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	if(::isatty(1))
	{
		eut->consoleDebug(true);
	}
	eut->setEOTS("\n");
	/*eut->nodelay(true);
	if(!eut->ok())
	{
		cerr << eut->getError() << endl;
		exit(255);
	}*/
	eut->atomicIO(true);
	eut->readTimeout(1,0);
	eut->writeTimeout(1,0);

	/*string version;
	if(!eut->controllerVersion(version))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Version: " << version << endl;
	bool manual;
	if(!eut->manual(&manual))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	
	cerr << "Manual: " << manual << endl;
	manual = false;
	if(!eut->manual(manual))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	if(!eut->manual(&manual))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Manual: " << manual << endl;*/

	/*unsigned short res;
	if(!eut->resolution(&res))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Resolution: " << res << endl;
	res = 1;
	if(!eut->resolution(res))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	if(!eut->resolution(&res))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Resolution: " << res << endl;*/
	double val;
	if(!eut->temperature(&val))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Temperature: " << val << endl;
	if(!eut->value((unsigned short) ::strtol(argv[1], 0, 10), &val))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Value: " << val << endl;
	/*val = ::strtol(argv[2], 0, 10);
	if(!eut->value((unsigned short) ::strtol(argv[1], 0, 10), val))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	if(!eut->value((unsigned short) ::strtol(argv[1], 0, 10), &val))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Value: " << val << endl;*/
	
	/*if(!eut->multiplier(&val))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Multi: " << val << endl;
	if(!eut->multiplier(val = 1))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	if(!eut->multiplier(&val))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Multi: " << val << endl;*/

	

	/*if(!eut->temperature(&val))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Temperature: " << val << endl;
	if(!eut->setPoint(&val))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Setpoint: " << val << endl;*/
	/*val = 25;
	if(!eut->setPoint(val))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	double val1;
	if(!eut->setPoint(&val1))
	{
		cerr << eut->getError() << endl;
		exit(255);
	}
	cerr << "Setpoint: " << val1 << endl;*/
	
	exit(0);	
}
