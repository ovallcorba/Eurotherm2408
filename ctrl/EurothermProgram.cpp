#include <ipc_exception.h>
#include <iostream>
#include <sstream>
#include <cstring>

#include "EurothermProgram.h"

const char* segtypekeys[] = {"SEG", "TYPE:END", "TYPE:RRATE", "TYPE:TRATE", "TYPE:DWELL", "TYPE:STEP", "TYPE:CALL", 0};

Segment::Segment()
{
	for(int i = 0; i < SEGSIZE; i++)
	{
		segData[i] = 0x8000;
	}
	segData[0] = 0xffff;
}



/*! 
 					constructor for Segment\n\n
					in - pn: program number of program this segment belongs to\n
					in - sn: segment number\n
					in - st: segment type\n
					in - gerr : pointer to error handling object

					check with Segment::ok() after construction
*/

Segment::Segment(unsigned short sn, unsigned short pn, unsigned short st)
{
	for(int i = 0; i < SEGSIZE; i++)
	{
		segData[i] = 0x8000;
	}
	segOk = false;
	segData[0] = 0xffff;
	progNumber(pn);
	segNumber(sn);
	segmentType(st);
}

/*! 
 	sets segment type
					
	in - type: unsigned short giving type 

	return:

	true: everything ok\n
	false: error, get description with Io::getError()

	for possible types cf. enum Segment::segTypes
		
*/



void Segment::segmentType(unsigned short type)
{
	if(type < SEND || type > SCALL)
	{
		segOk = false;
		stringstream tmp;
		tmp << "Invalid segment type for program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	segData[0] = type;
	segOk = true;
}

/*! 
 	sets segment number
					
	in - sn: unsigned short giving segment number

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/

void Segment::segNumber(unsigned short sn)
{
	/*if(sn > maxsegs)
	{
		*glErr << "Invalid segment number for program " << progNum << ": " << sn << endl;
		return segOk = false;
	}*/
	segNum = sn;
	segOk = true;
}

/*! 
 	gets setpoint for this segment, if applicable
					
	in - sp: address of unsigned short to store setpoint

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/

void Segment::setPoint(unsigned short* sp)
{
	validSegment();
	if(segmentType() == SDWELL || segmentType() == SCALL || segmentType() == SEND)
	{
		segOk = false;
		stringstream tmp;
		tmp << "No setpoint for segment type " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	*sp = segData[1];
	segOk = true;
}


/*! 
 	sets setpoint for this segment, if applicable
					
	in - sp: unsigned short containing setpoint

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/


void Segment::setPoint(unsigned short sp)
{
	validSegment();
	if(segmentType() == SDWELL || segmentType() == SCALL || segmentType() == SEND)
	{
		segOk = false;
		stringstream tmp;
		tmp <<  "No setpoint for segment type " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	segData[1] = sp;
	segOk = true;

}

/*! 
 	gets endpower for this segment, if applicable
					
	in - ep: address of unsigned short to store endpower

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/

/*bool Segment::endPower(unsigned short* ep)
{
	validSegment();
	if(segmentType() != SEND)
	{
		*glErr << "No end power for " << segmentType() << " in program " << progNum << " , segment " << segNum << endl;
		return segOk = false;
	}
	*ep = segData[1];
	return segOk = true;

}*/

/*! 
 	sets endpower for this segment, if applicable
					
	in - ep: unsigned short containing endpower

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/

/*bool Segment::endPower(unsigned short ep)
{
	validSegment();
	if(segmentType() != SEND)
	{
		*glErr << "No end power for " << segmentType() << " in program " << progNum << " , segment " << segNum << endl;
		return segOk = false;
	}
	segData[1] = ep;
	return segOk = true;
}*/

/*! 
 	gets duration for this segment, if applicable
					
	in - du: address of unsigned short to store duration

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/


void Segment::duration(unsigned short *du)
{
	validSegment();
	if(segmentType() != SDWELL && segmentType() != STRATE)
	{
		segOk = false;
		stringstream tmp;
		tmp <<  "No duration for " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	*du = segData[2];
	segOk = true;
}

/*! 
 	sets duration for this segment, if applicable
					
	in - du: unsigned short containing duration

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/

void Segment::duration(unsigned short du)
{
	validSegment();
	if(segmentType() != SDWELL && segmentType() != STRATE)
	{
		segOk = false;
		stringstream tmp;
		tmp <<  "No duration for " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	segData[2] = du;
	segOk = true;
}
/*! 
 	gets rate for this segment, if applicable
					
	in - ra: address of unsigned short to store rate

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/
void Segment::rate(unsigned short *ra)
{
	validSegment();
	if(segmentType() != SRRATE)
	{
		segOk = false;
		stringstream tmp;
		tmp <<  "No rate for " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	*ra = segData[2];
	segOk = true;
}
/*! 
 	sets rate for this segment, if applicable
					
	in - ra: unsigned short containing rate

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/
void Segment::rate(unsigned short ra)
{
	validSegment();
	if(segmentType() != SRRATE)
	{
		segOk = false;
		stringstream tmp;
		tmp <<  "No rate for " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	segData[2] = ra;
	segOk = true;
}
/*! 
 	gets logic ops for this segment, if applicable
					
	in - op: address of unsigned short to store  logic ops

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/
void Segment::logicOps(unsigned short* op)
{
	validSegment();
	if(segmentType() == SCALL)
	{
		segOk = false;
		stringstream tmp;
		tmp <<  "No logic ops for " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	*op = segData[4];
	segOk = true;
}
/*! 
 	sets logic ops for this segment, if applicable
					
	in - op: unsigned short to containing logic ops

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/


void Segment::logicOps(unsigned short op)
{
	validSegment();
	if(segmentType() == SCALL)
	{
		segOk = false;
		stringstream tmp;
		tmp << "No logic ops for " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	segData[4] = op;
	segOk = true;
}


/*! 
 	gets program number for this segment
					
	in - pn: address of unsigned short to store program number

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/

void Segment::programNumber(unsigned short *pn)
{
	validSegment();
	if(segmentType() != SCALL)
	{
		segOk = false;
		stringstream tmp;
		tmp << "No program number for " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	*pn = segData[3];
	segOk = true;
}

/*! 
 	sets program number for this segment
					
	in - pn: unsigned short containing program number

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/
void Segment::programNumber(unsigned short pn)
{
	validSegment();
	if(segmentType() != SCALL)
	{
		segOk = false;
		stringstream tmp;
		tmp << "No program number for " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	segData[3] = pn;
	segOk = true;
}

/*! 
 	gets endtype for this segment, if applicable
					
	in - et: address of unsigned short to store endtype

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/

void Segment::endType(unsigned short *et)
{
	validSegment();
	if(segmentType() != SEND)
	{
		segOk = false;
		stringstream tmp;
		tmp << "No end type for " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	*et = segData[3];
	segOk = true;
}

/*! 
 	sets endtype for this segment, if applicable
					
	in - et: unsigned short containing endtype

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/


void Segment::endType(unsigned short et)
{
	validSegment();
	if(segmentType() != SEND)
	{
		segOk = false;
		stringstream tmp;
		tmp << "No end type for " << segmentType() << " in program " << progNum << " , segment " << segNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	segData[3] = et;
	segOk = true;
}

/*! 
 	gets call cycles for this segment, if applicable
					
	in - cc: address of unsigned short to store call cycles

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/

void Segment::callCycles(unsigned short *cc)
{
	validSegment();
	if(segmentType() !=	SCALL)
	{
		segOk = false;
		stringstream tmp;
		tmp << "No call cycles for " << segmentType() << " in program " << progNum << " , segment " << segNum << endl;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	*cc = segData[4];
	segOk = true;
}
/*! 
 	sets call cycles for this segment, if applicable
					
	in - cc: unsigned short containing call cycles

	return:

	true: everything ok\n
	false: error, get description with Io::getError()
		
*/

void Segment::callCycles(unsigned short cc)
{
	validSegment();
	if(segmentType() !=	SCALL)
	{
		segOk = false;
		stringstream tmp;
		tmp << "No call cycles for " << segmentType() << " in program " << progNum << " , segment " << segNum << endl;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	segData[4] = cc;
	segOk = true;
}

/*!
	gets numeric representation for segment type given as string in stype\n

	in - stype : pointer to char with segment type given as string 
	
	in - ntype : address of unsigned short to store numeric representation
	return:

	true: everything ok\n
	false: error, unknown segment type

	valid strings are:\n
	"TYPE:END", "TYPE:RRATE", "TYPE:TRATE", "TYPE:DWELL", "TYPE:STEP", "TYPE:CALL"

*/


bool Program::segType(string stype, unsigned short* ntype)
{
	for(int i = 0; segtypekeys[i]; i++)
	{
		if(!strcmp(segtypekeys[i], stype.c_str()))
		{
			*ntype = i - 1;
			return true;
		}
	}
	return false;
}

void Segment::validSegment()
{
	if(segmentType() == 0xffff)
	{
		segOk = false;
		stringstream tmp;
		tmp << "No segment type set for program " << progNum << " , segment " << segNum << endl;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	segOk = true;
}

/*! 
 					constructor for Program, constructs an empty program
*/

Program::Program(unsigned short maxsegs)
{
	for(int i = 0; i < SEGSIZE; i++)
	{
		genData[i] = 0x8000;
	}	
	progNum = 0xffff;
	progOk	= true;
	for(int i = 0; i < maxsegs; i++)
	{
		segments.push_back((Segment*) 0);
	}
}

/*! 
 					constructor for Program\n\n
					in - pn: program number of program this segment belongs to\n
					in - ht: holdback type\n
					in - hv: holdback value\n
					in - ru: ramp units\n
					in - du: dwell units\n
					in - pc: program cycles
					in - gerr : pointer to error handling object

					check with Program::ok() after construction
*/


Program::Program(unsigned short maxsegs, unsigned short pn, unsigned short ht, unsigned short hv, unsigned short ru, unsigned short du, unsigned short pc) : progNum(pn)
{
	for(int i = 0; i < SEGSIZE; i++)
	{
		genData[i] = 0x8000;
	}
	progOk	= true;
	//progNumber(pn);
	holdBackType(ht);
	holdBackValue(hv);
	rampUnits(ru);
	dwellUnits(du);
	cycles(pc);
	for(int i = 0; i < maxsegs; i++)
	{
		segments.push_back((Segment*) 0);
	}

}

Program::~Program()
{
	vector<Segment*>::iterator it;
	for(it = segments.begin(); it < segments.end(); it++)
	{
		if(*it)
		{
			delete *it;
		}
	}
}


/*!
	adds segment to program\n

	in - seg : pointer to segment object
	
	return:

	true: everything ok\n
	false: error, get description with Io::getError()

*/

void Program::addSegment(Segment* seg)
{
	if(segments.at(seg->segNumber() - 1))
	{
		progOk = false;
		stringstream tmp;
		tmp << "Segment " << seg->segNumber() << " already exist in program " << progNumber();
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	segments.at(seg->segNumber() - 1) = seg;
	progOk = true;
}

/*!
	sets program number for this program\n

	in - pn : unsigned short containing program number
	
	return:

	true: everything ok\n
	false: error, get description with Io::getError()

*/

/*bool Program::progNumber(unsigned short pn)
{
	if(pn > MAXPROGS)
	{
		*glErr << "Invalid program number " << pn << endl;
		return progOk = false;
	}	
	progNum = pn;
	return  progOk = true;
}*/

/*!
	sets holdback type for this program\n

	in - type : unsigned short containing holdback type
	
	return:

	true: everything ok\n
	false: error, get description with Io::getError()

*/

void Program::holdBackType(unsigned short type) 
{
	if(type < HBNONE || type > HBBAND)
	{
		progOk = false;
		stringstream tmp;
		tmp << "Invalid holdback type for program " << progNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	genData[0] = type;
	progOk = true;
}

/*!
	gets holdback value for this program\n
	return: \n holdback value
*/

unsigned short Program::holdBackValue()
{
	return genData[1];
}

/*!
	sets holdback value for this program\n

	in - type : unsigned short containing holdback value
	
	return:

	true: everything ok\n
	false: error, get description with Io::getError()

*/


bool Program::holdBackValue(unsigned short hv) 
{
	/*if(hv != 0)
	{
		*glErr << "Invalid holdback value for program " << progNum << endl;
		return progOk = false;
	}*/
	genData[1] = hv;
	return progOk = true;
}


/*!
	sets ramp units for this program\n

	in - unit : unsigned short containing ramp units
	
	return:

	true: everything ok\n
	false: error, get description with Io::getError()

*/

void Program::rampUnits(unsigned short unit)
{
	if(unit < RDSEC || unit > RDHOURS)
	{
		progOk = false;
		stringstream tmp;
		tmp << "Invalid ramp unit for program " << progNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	genData[2] = unit;
	progOk = true;
}


/*!
	sets dwell units for this program\n

	in - unit : unsigned short containing dwell units
	
	return:

	true: everything ok\n
	false: error, get description with Io::getError()

*/

void Program::dwellUnits(unsigned short unit) 
{
	if(unit < RDSEC || unit > RDHOURS)
	{
		progOk = false;
		stringstream tmp;
		tmp << "Invalid dwell unit for program " << progNum;
		throw(IpcException(tmp.str(),  __FILE__, __LINE__));
	}
	genData[3] = unit;
	progOk = true;
}



/*!
	gets numeric representation for dwell / ramp units given as string in ttype\n

	in - ttype : pointer to char with dwell / ramp units given as string 
	
	in - ntype : address of unsigned short to store numeric representation of dwell / ramp units
	return:

	true: everything ok\n
	false: error, unknown unit type

	valid strings are:\n
	"SEC", "MIN", "HOUR"

*/


bool Program::tunits(const char* ttype, unsigned short* ntype)
{
	const char* utypes[] = {"SEC", "MIN", "HOUR", 0};
	for(unsigned short i = 0; utypes[i]; i++)
	{
		if(!strcmp(utypes[i], ttype))
		{
			*ntype = i;
			return true;
		}
	}
	return false;
}

/*!
	gets numeric representation for holdback type given as string in htype\n

	in - ttype : pointer to char with holdback type given as string 
	
	in - ntype : address of unsigned short to store numeric representation of holdback type
	return:

	true: everything ok\n
	false: error, unknown holdback type

	valid strings are:\n
	"NONE", "LOW", "HIGH", "BAND"

*/

bool Program::hbtype(const char* htype, unsigned short* ntype)
{
	const char* htypes[] = {"NONE", "LOW", "HIGH", "BAND", 0};
	for(unsigned short i = 0; htypes[i]; i++)
	{
		if(!strcmp(htypes[i], htype))
		{
			*ntype = i;
			return true;
		}
	}
	return false;
}
